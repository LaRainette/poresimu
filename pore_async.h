/*
 * pore_async.h
 *
 * Copyright 2012 Félix Cantournet <felix.cantournet@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
#ifndef PORE_ASYNC_H
#define PORE_ASYNC_H

#include <memory>
#include <unordered_set>

using namespace std;
class Pore_async
{
    public:
//        static std::vector<Pore_async *> Connected;
        Pore_async();
        Pore_async(float radius_, int x_, int y_, int z_);
        void setRadius(float radius_);
        void setSaturated(bool saturated_);
        void addNeighbourg(shared_ptr<Pore_async> &neighbourg_);
        int getNumberOfNeighbours();
        unordered_set<shared_ptr<Pore_async> > Neighbours;
        bool getSaturated();
        float getRadius();
        int getx();
        int gety();
        int getz();

    private:
        int x, y, z;
        float Radius;
        bool Saturated;

        /* add your private declarations */
};

#endif /* PORE_ASYNC_H */
