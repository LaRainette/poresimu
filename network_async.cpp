/*
 * network_async.cpp
 *
 * Copyright 2012 Félix Cantournet <felix.cantournet@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

// boost lib for multidimensional arrays.
#include "boost/multi_array.hpp"

#include "network_async.h"
#include "pore_async.h"

#include <memory>
#include <ctime>
#include <cmath>
#include <vector>
#include <stack>
#include <unordered_set>
#include <QDebug>

typedef std::shared_ptr<Pore_async> PorePtr;
typedef boost::multi_array<PorePtr, 3> PorePtrGrid;
typedef std::vector< PorePtr > PorePtrVector;
typedef std::unordered_set< PorePtr > PorePtrSet;
typedef std::stack< PorePtr > PorePtrStack;

const double TENSION = 0.072;
const double COSTHETA = cos(0);

Network_Async::Network_Async(boost::multi_array<float, 3> &radiuses_):
    Size(radiuses_.shape()[0]),
    Grid(boost::extents[Size][Size][Size])
{
    //~ for (int w=0; w!=(Size**3); ++w)
        //~ List.push_back(new Pore_async)
    //PorePtrGrid tempgrid(boost::extents[size_][size_][size_]);
    // create initial pores
    for(int i = 0; i != Size; ++i)
        for(int j = 0; j != Size; ++j)
            for(int k = 0; k != Size; ++k) {
                AllThePores.push_back(PorePtr(new Pore_async(radiuses_[i][j][k], i, j, k)));
                PorePtr temp = AllThePores.back();
                Grid[i][j][k] = temp;
                qDebug() << "Pore : " << i << j << k;
                qDebug() << "Saturated : " << AllThePores.back()->getSaturated();
                qDebug() << "Radius : " << AllThePores.back()->getRadius()<< endl;
            }
    //set interface to air
    setInterfaceToAir();
    // create neigbours lists
    createNeighboursLists();
}

void Network_Async::createNeighboursLists()
{
//    AllThePores[0]->addNeighbourg(AllThePores[1]);
//    AllThePores[0]->addNeighbourg(AllThePores[1]);
    //This first double loop handles the limit cases (outer indices)
        for(int i = 0; i != Size; ++i)
            for(int j = 0; j != Size; ++j) {
                if (i>0) {
                    Grid[i][j][0]->addNeighbourg(Grid[i-1][j][0]);
                    Grid[i][j][Size-1]->addNeighbourg(Grid[i-1][j][Size-1]);
                    Grid[i][0][j]->addNeighbourg(Grid[i-1][0][j]);
                    Grid[0][i][j]->addNeighbourg(Grid[0][i-1][j]);
                    Grid[i][Size-1][j]->addNeighbourg(Grid[i-1][Size-1][j]);
                    Grid[Size-1][i][j]->addNeighbourg(Grid[Size-1][i-1][j]);
                }
                if (j>0) {
                    Grid[i][j][0]->addNeighbourg(Grid[i][j-1][0]);
                    Grid[i][j][Size-1]->addNeighbourg(Grid[i][j-1][Size-1]);
                    Grid[i][Size-1][j]->addNeighbourg(Grid[i][Size-1][j-1]);
                    Grid[0][i][j]->addNeighbourg(Grid[0][i][j-1]);
                    Grid[i][0][j]->addNeighbourg(Grid[i][0][j-1]);
                    Grid[Size-1][i][j]->addNeighbourg(Grid[Size-1][i][j-1]);
                }
                if (i<(Size-1)) {
                    Grid[i][j][0]->addNeighbourg(Grid[i+1][j][0]);
                    Grid[i][j][Size-1]->addNeighbourg(Grid[i+1][j][Size-1]);
                    Grid[i][0][j]->addNeighbourg(Grid[i+1][0][j]);
                    Grid[i][Size-1][j]->addNeighbourg(Grid[i+1][Size-1][j]);
                    Grid[0][i][j]->addNeighbourg(Grid[0][i+1][j]);
                    Grid[Size-1][i][j]->addNeighbourg(Grid[Size-1][i+1][j]);
                }
                if (j<(Size-1)) {
                    Grid[i][j][0]->addNeighbourg(Grid[i][j+1][0]);
                    Grid[i][j][Size-1]->addNeighbourg(Grid[i][j+1][Size-1]);
                    Grid[i][0][j]->addNeighbourg(Grid[i][0][j+1]);
                    Grid[i][Size-1][j]->addNeighbourg(Grid[i][Size-1][j+1]);
                    Grid[0][i][j]->addNeighbourg(Grid[0][i][j+1]);
                    Grid[Size-1][i][j]->addNeighbourg(Grid[Size-1][i][j+1]);
                }
                //this gets executed on all the loops
                Grid[i][j][0]->addNeighbourg(Grid[i][j][1]);
                Grid[i][j][Size-1]->addNeighbourg(Grid[i][j][Size-2]);
                Grid[i][0][j]->addNeighbourg(Grid[i][1][j]);
                Grid[i][Size-1][j]->addNeighbourg(Grid[i][Size-2][j]);
                Grid[0][i][j]->addNeighbourg(Grid[1][i][j]);
                Grid[Size-1][i][j]->addNeighbourg(Grid[Size-2][i][j]);
        }
        // this triple loop handles the rest
        for(int i = 1; i != (Size-1); ++i)
            for(int j = 1; j != (Size-1); ++j)
                for(int k = 1; k != (Size-1); ++k)  {
                    Grid[i][j][k]->addNeighbourg(Grid[i+1][j][k]);
                    Grid[i][j][k]->addNeighbourg(Grid[i-1][j][k]);
                    Grid[i][j][k]->addNeighbourg(Grid[i][j+1][k]);
                    Grid[i][j][k]->addNeighbourg(Grid[i][j-1][k]);
                    Grid[i][j][k]->addNeighbourg(Grid[i][j][k+1]);
                    Grid[i][j][k]->addNeighbourg(Grid[i][j][k-1]);
        }
}

void Network_Async::setPressure(int64_t pressure_)
{
    Pressure = pressure_;
    MinRadiusToDesaturate = (COSTHETA*4*TENSION)/(double(Pressure)/1000000.0);
}

int64_t Network_Async::getPressure() {return Pressure;}

void Network_Async::setInterfaceToAir()
{
    this->TotalConnected = 0;
    for(int j = 0; j != Size; ++j) {
        PorePtr temp = AllThePores[j];
        this->Connected.insert(temp);
        this->TotalConnected++;
        qDebug() << "Inserted in Connected : Pore : Radius = " << AllThePores[j]->getRadius() << " Saturated = " << AllThePores[j]->getSaturated();
    }
}

void Network_Async::simulateDesaturation(int startP, int stopP, int stepP)
{
    setPressure(startP);
    while (getPressure() < stopP)
    {
        Tested.clear();
        qDebug() << "Setting Pressure to : " << Pressure;
        qDebug() << "Corresponding to a minimum Radius of : " << MinRadiusToDesaturate;
        setPressure(getPressure()+stepP);
        for (PorePtrSet::iterator it=Connected.begin(); it!=Connected.end(); ++it)
        {
            PorePtr temp1 = (*it);
            PorePtr temp2 = temp1;
            Stack.push(temp1);
            Tested.insert(temp2);
        }
        while (!Stack.empty())
        {
            PorePtr tempPorePtr = Stack.top();
            Stack.pop();
            if (tempPorePtr->getRadius() > MinRadiusToDesaturate)
            {
                desaturatePore(tempPorePtr);
                qDebug() << "Desaturated Pore";
            }
        }
    }
}
int Network_Async::calculateSaturation()
{
    int counter = 0;
    for (PorePtrVector::iterator it=AllThePores.begin(); it!=AllThePores.end(); ++it)
        if (!(*it)->getSaturated()) {counter++;}
    return counter;
}

int Network_Async::getCardinality()
{
    int cardinality = 0;
    for (PorePtrVector::iterator it=AllThePores.begin(); it!=AllThePores.end(); ++it)
        cardinality += (*it)->getNumberOfNeighbours();
    return cardinality;
}

void Network_Async::desaturatePore(PorePtr poreptr)
{
    poreptr->setSaturated(false);
    for (PorePtrSet::iterator it=poreptr->Neighbours.begin(); it!=poreptr->Neighbours.end(); ++it)
    {
        PorePtr temp1 = (*it);
        if (Tested.count(temp1) == 0)
        {
        PorePtr temp2 = temp1;
        PorePtr temp3 = temp2;
        this->Stack.push(temp2);
        this->Tested.insert(temp3);
        }
        this->Connected.insert(temp1);
    }
}
int Network_Async::calculateDistance(PorePtr ptr1, PorePtr ptr2)
{
    int dx = abs((ptr1->getx() - ptr2->getx()));
    int dy = abs((ptr1->gety() - ptr2->gety()));
    int dz = abs((ptr1->getz() - ptr2->getz()));
    return dx+dz+dy;
}

void Network_Async::testcardinality()
{
    int voisins3 = 0;
    int voisins4 = 0;
    int voisins5 = 0;
    int voisins6 = 0;
    for (PorePtrVector::iterator it=AllThePores.begin(); it!=AllThePores.end(); ++it)
        if ((*it)->getNumberOfNeighbours() == 3) {voisins3++;}
        else if ((*it)->getNumberOfNeighbours() == 4) {voisins4++;}
        else if ((*it)->getNumberOfNeighbours() == 5)
        {
            voisins5++;
            if ((((*it)->getx() == 0) | ((*it)->getx() == (Size-1))) && (((*it)->gety() == 0) | ((*it)->gety() == (Size-1))))
            {
                qDebug() << "found one !";
                qDebug() << "x = " << (*it)->getx() << " y = " << (*it)->gety() << " z = " << (*it)->getz();
            }
            if ((((*it)->getx() == 0) | ((*it)->getx() == (Size-1))) && (((*it)->getz() == 0) | ((*it)->getz() == (Size-1))))
            {
                qDebug() << "found one !";
                qDebug() << "x = " << (*it)->getx() << " y = " << (*it)->gety() << " z = " << (*it)->getz();
            }
            if ((((*it)->getz() == 0) | ((*it)->getz() == (Size-1))) && (((*it)->gety() == 0) | ((*it)->gety() == (Size-1))))
            {
                qDebug() << "found one !";
                qDebug() << "x = " << (*it)->getx() << " y = " << (*it)->gety() << " z = " << (*it)->getz();
            }

        }
        else if ((*it)->getNumberOfNeighbours() == 6) {voisins6++;}
        else {qDebug()<< "Someone has " << (*it)->getNumberOfNeighbours() << "neighbours";}
    qDebug() << voisins3 << "Pores have 3 neighbors";
    qDebug() << voisins4 << "Pores have 4 neighbors";
    qDebug() << voisins5 << "Pores have 5 neighbors";
    qDebug() << voisins6 << "Pores have 6 neighbors";

    for (PorePtrVector::iterator it1=AllThePores.begin(); it1!=AllThePores.end(); ++it1)
        for (PorePtrSet::iterator it2=(*it1)->Neighbours.begin(); it2!=(*it1)->Neighbours.end(); ++it2)
            if (calculateDistance((*it1), (*it2)) > 1)
            {
                qDebug() << "Found a Pore that has a Neighbour too far :";
                qDebug() << "x = " << (*it1)->getx() << " y = " << (*it1)->gety() << " z = " << (*it1)->getz();
                qDebug() << "Has Neighbour at : ";
                qDebug() << "x = " << (*it2)->getx() << " y = " << (*it2)->gety() << " z = " << (*it2)->getz();
            }
}
