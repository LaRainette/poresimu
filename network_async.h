/*
 * network_async.h
 *
 * Copyright 2012 Félix Cantournet <felix.cantournet@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef NETWORK_ASYNC_H
#define NETWORK_ASYNC_H
#include "boost/multi_array.hpp"
#include <memory>
#include "pore_async.h"
#include <unordered_set>
#include <vector>
#include <stack>

typedef std::shared_ptr<Pore_async> PorePtr;
typedef std::vector<PorePtr> PorePtrVector;
typedef std::unordered_set< PorePtr > PorePtrSet;
typedef boost::multi_array<PorePtr, 3> PorePtrGrid;
typedef std::stack< PorePtr > PorePtrStack;


class Network_Async
{
    public:
        Network_Async(boost::multi_array<float, 3> &radiuses_);
        void setPressure(int64_t pressure_);
        int64_t getPressure();
        void setInterfaceToAir();
        void simulateDesaturation(int startP, int stopP, int stepP);
        int getCardinality();
        void desaturatePore(PorePtr poreptr);
        void testcardinality();
        int calculateSaturation();

    private:
        int Size;
        PorePtrSet Connected;
        PorePtrVector AllThePores;
        PorePtrGrid Grid;
        PorePtrStack Stack;
        PorePtrSet Tested; //usefull to keep track of the Pores that we have
        //already tried to empty but couldn't because of R <Rmin.
        //This way you don't try several times for P given.
        int64_t Pressure;
        int TotalConnected;
        double MinRadiusToDesaturate;
        void createNeighboursLists();
        int calculateDistance(PorePtr ptr1, PorePtr ptr2);
        /* add your private declarations */
};

#endif /* NETWORK_ASYNC_H */
