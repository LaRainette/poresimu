#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "network_async.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButtonGenerate_clicked();
    void on_pushButtonRun_clicked();
    
private:
    Ui::MainWindow *ui;
    Network_Async *model;
};

#endif // MAINWINDOW_H
