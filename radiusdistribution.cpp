#include "radiusdistribution.h"
#include "boost/multi_array.hpp"

// 3 boost/random libs necessary to generate random radiuses.
#include "boost/random/mersenne_twister.hpp"
#include "boost/random/normal_distribution.hpp"
#include "boost/random/variate_generator.hpp"

typedef boost::random::mt19937 ENG;
typedef boost::normal_distribution<double> DIST;   // Normal Distribution
typedef boost::variate_generator<ENG,DIST> GEN;    // Variate generator

RadiusDistribution::RadiusDistribution(int size_):
    Radiuses(boost::extents[size_][size_][size_])
{
    ENG eng;
    //eng.seed(static_cast<unsigned int>(std::time(0)));
    DIST dist(0.6,0.3);
    GEN gen(eng, dist);

    for(int i = 0; i != size_; ++i)
        for(int j = 0; j != size_; ++j)
            for(int k = 0; k != size_; ++k) {
                Radiuses[i][j][k] = gen();
            }

}
