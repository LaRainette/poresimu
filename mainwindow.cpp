#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>
#include "network_async.h"
#include "radiusdistribution.h"
#include <math.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->ui->lineEditSize->setText(QString("10"));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButtonGenerate_clicked()
{
    int modelsize = this->ui->lineEditSize->text().toInt();
    RadiusDistribution distribution = RadiusDistribution(modelsize);
    model = new Network_Async(distribution.Radiuses);
    int temp = this->model->getCardinality();
    this->ui->lcdNumberAsync->display(temp);
    int tempo = (8*3+12*(modelsize-2)*4+pow((modelsize-2),2)*6*5+pow((modelsize-2),3)*6);
    this->ui->lcdNumberBoost->display(tempo);
}

void MainWindow::on_pushButtonRun_clicked()
{
    model->simulateDesaturation(300000, 500000, 50000);
    int sat = model->calculateSaturation();
    this->ui->lcdNumberAsync->display(sat);
}
