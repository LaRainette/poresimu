#ifndef RADIUSDISTRIBUTION_H
#define RADIUSDISTRIBUTION_H

#include "boost/multi_array.hpp"

typedef boost::multi_array<float, 3> RadiusesGrid;

class RadiusDistribution
{
public:
    RadiusDistribution(int size_);
    RadiusesGrid Radiuses;
};

#endif // RADIUSDISTRIBUTION_H
