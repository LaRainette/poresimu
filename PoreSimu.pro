#-------------------------------------------------
#
# Project created by QtCreator 2012-12-24T17:17:20
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PoreSimu
TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    network_async.cpp \
    pore_async.cpp \
    radiusdistribution.cpp

HEADERS  += mainwindow.h \
    network_async.h \
    pore_async.h \
    radiusdistribution.h

FORMS    += mainwindow.ui
