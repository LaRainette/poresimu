/*
 * pore_async.cpp
 *
 * Copyright 2012 Félix Cantournet <felix.cantournet@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "pore_async.h"
#include <unordered_set>
#include <memory>

#include <ctime>
#include <iostream>
#include <cmath>

using namespace std;

Pore_async::Pore_async():
    Radius(0.5),
    Saturated(true)

{

}

Pore_async::Pore_async(float radius_, int x_, int y_, int z_):
    x(x_),
    y(y_),
    z(z_),
    Radius(radius_),
    Saturated(true)
{

}

void Pore_async::setSaturated(bool saturated_) {Saturated = saturated_;}
bool Pore_async::getSaturated() {return Saturated;}
void Pore_async::setRadius(float radius_) {Radius = radius_;}
float Pore_async::getRadius() {return Radius;}


void Pore_async::addNeighbourg(shared_ptr<Pore_async> &neighbour_)
{
    shared_ptr<Pore_async> temp = neighbour_;
    Neighbours.insert(temp);
}

int Pore_async::getNumberOfNeighbours()
{
    unordered_set<shared_ptr<Pore_async> >::size_type i;
    i = Neighbours.size();
    return i;
}

int Pore_async::getx() {return x;}
int Pore_async::gety() {return y;}
int Pore_async::getz() {return z;}
